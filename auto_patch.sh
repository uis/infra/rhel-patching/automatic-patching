#!/bin/bash

# Define global variables
CONF_FILE="./auto_patch.cfg"
LOG_FILE="/var/log/auto-patch.log"
DATE_FORMAT="%Y-%m-%dT%H:%M:%SZ"
REPORTS_DIR="./reports"
TMP_DIR="/tmp/auto-patch/$(date -u +$DATE_FORMAT)"

# TMP_DIR and REPORT_DIR must exist
mkdir -p $TMP_DIR
mkdir -p $REPORTS_DIR

###
# Description: log a message.
# Args: severity (string): should be: "ERROR", "WARN", or "INFO".
#       log message (string).
# Return: none.
###
logit(){
    printf "%s %s %-7s %s\n" "`date -u +$DATE_FORMAT`" "[$HOSTNAME]" "[$1]" "$(echo -e $2)" | tee -a $LOG_FILE
}

###
# Description: Get default IP address of the server.
# Return: 0 for success, 1 on error.
# Output: default_iface_ip (string).
###
get_default_iface_ip(){
  local default_iface=$(awk '$2 == 00000000 { print $1 }' /proc/net/route); local r1=$?
  default_iface_ip=$(ip addr show dev "$default_iface" | awk '$1 == "inet" { print $2 }'); local r2=$?

  if [ $r1 -ne 0 ] || [ $r2 -ne 0 ] || [ -z $default_iface_ip ]; then
    logit "ERROR" "Cannot get default interface IP address."
    default_iface_ip="undefined"
    return 1
  fi

  return 0
}

###
# Description: Read the auto_patch config file passed in argument.
# Args: none.
# Return: 0 for success, 1 on error.
###
read_config_file(){
  source $CONF_FILE

  if [ -z ${intolerant_services+x} ] || [ -z ${exclude+x} ] || [ -z ${maillist+x} ] \
     || [ -z ${dry_run+x} ] || [ -z ${reboot_allowed+x} ] || [ -z ${auto_reboot+x} ] \
     || [ -z ${auto_service_restart+x} ] || [ -z ${reboot_by_ansible+x} ] \
     || [ -z ${yum_update_opts+x} ]; then
    logit "ERROR" "Missing variables in config file $CONF_FILE."
    return 1
  fi

  return 0
}

###
# Description: Run yum update.
# Args: none.
# Return: 0 for success, 1 on error.
###
yum_update(){
  yum_update_file=$(mktemp $TMP_DIR/yum_update.XXXXXXX)

  yum clean all > /dev/null 2>&1; local clean=$?
  rm -rf /var/cache/yum  > /dev/null 2>&1; local cache=$?
  yum update -y $yum_update_opts > $yum_update_file 2>&1; local update=$?

  if [ $clean -ne 0 ] || [ $cache -ne 0 ] || [ $update -ne 0 ]; then
    logit "ERROR" "Yum update failed. yum clean all -> $clean | rm -rf /var/cache/yum -> $cache | yum update -> $update.\nyum update -y ->\n$(cat $yum_update_file)"
  fi

  YUM_UPDATE_RESULT=`expr $clean + $cache + $update`

  return $YUM_UPDATE_RESULT
}

###
# Description: Reinstall m2crypto as a workaround of a bug with 
# yum plugins not being able to be imported after certain packages are updated.
# Args: none.
# Return: 0 if tracer installed, 1, otherwise.
###
reinstall_m2crypto(){
  yum reinstall m2crypto -y > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    logit "ERROR" "yum reinstall m2crypto failed."
    return 1
  fi

  return 0
}

###
# Description: Check if tracer is installed.
# Args: none.
# Return: 0 if tracer installed, 1, otherwise.
###
is_tracer_installed(){
  rpm -qa | grep katello-host-tools-tracer > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    logit "ERROR" "katello-host-tools-tracer package is not installed."
    return 1
  fi

  return 0
}

###
# Description: Upload tracer data.
# Args: none.
# Return: 0 for success, 1 on error.
###
upload_tracer_data(){
  is_tracer_installed
  if [ $? -ne 0 ]; then
    return 1
  fi

  katello-tracer-upload > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    logit "ERROR" "Cannot upload tracer data."
    return 1
  fi

  return 0
}

###
# Description: Run tracer.
# Args: none.
# Return: 0 for success, 1 on error.
###
run_tracer(){
  is_tracer_installed
  if [ $? -ne 0 ]; then
    return 1
  fi

  tracer_output_file=$(mktemp $TMP_DIR/tracer_output.XXXXXXX)

  tracer > $tracer_output_file 2>&1; local tracer_return=$?
  if [ $? -ne 0 ]; then
    logit "ERROR" "Tracer failed."
    return 1
  fi

  return 0
}

###
# Description: Read tracer output.
# Args: none.
# Return: 0 for success, 1 on error.
###
read_tracer_output(){
  # run tracer
  run_tracer
  if [ $? -ne 0 ]; then
    return 1
  fi

  # determine if server reboot is required
  if grep -q "requiring reboot" $tracer_output_file
  then
    REBOOT_REQUIRED=1
  else
    REBOOT_REQUIRED=0
  fi

  # get the services to restart with systemctl
  systemctl_restart_services_file=$(mktemp $TMP_DIR/systemctl_restart_services.XXXXXXX)
  cat "$tracer_output_file" | grep "systemctl restart " | sed "s/.*systemctl restart //g" > "$systemctl_restart_services_file"

  # get the services to be restarted manually
  manual_restart_services_file=$(mktemp $TMP_DIR/manual_restart_services.XXXXXXX)
  sed -n '/.*These applications manually:/,/^[[:space:]]*$/p' $tracer_output_file | sed '1d;$d' | awk '{$1=$1};1' > "$manual_restart_services_file"
  sed -i -e 's/^/    - /' $manual_restart_services_file

  # determine if any automatic service restart is required
  if grep -q "systemctl restart " $tracer_output_file
  then
    SERVICE_RESTART_REQUIRED=1
  else
    SERVICE_RESTART_REQUIRED=0
  fi

  # determine if any manual service restart is required
  if grep -q "These applications manually:" $tracer_output_file
  then
    MANUAL_SERVICE_RESTART_REQUIRED=1
  else
    MANUAL_SERVICE_RESTART_REQUIRED=0
  fi

  # determine if session restart (logging out and in) is required
  if grep -q "requiring restart of your session" $tracer_output_file
  then
    SESSION_RESTART_REQUIRED=1
  else
    SESSION_RESTART_REQUIRED=0
  fi

  # mark services intolerant to restart
  mark_intolerant_services $systemctl_restart_services_file

  return $?
}

###
# Description: Comment the lines containing the names of services intolerant to restart.
# Args: $systemctl_restart_services_file
# Return: 0 for success, 1 on error.
###
mark_intolerant_services(){
  if [ -z "$1" ]; then
    logit "ERROR" "Cannot find tracer output file."
    return 1
  fi

  if [ -z "$intolerant_services" ]; then
    return 0
  fi

  local services_to_mark="($(echo $intolerant_services | sed -e 's/\s\+/|/g'))"

  # update $systemctl_restart_services_file
  sed -i -E "s/.*$services_to_mark/#&/g" $systemctl_restart_services_file

  return $?
}

###
# Description: Restart services if required.
# Args: none.
# Return: 0 for success, 1 on error.
###
restart_service_if_required(){
  if [ $SERVICE_RESTART_REQUIRED -eq 1 ] && [ $AUTO_SERVICE_RESTART -eq 1 ]; then
    logit "INFO" "Restarting services: $(cat $systemctl_restart_services_file)"
    restart_services $systemctl_restart_services_file

    if [ $? -eq 0 ]; then
      logit "INFO" "Service restart succeeded."
    else
      logit "ERROR" "All or some services cannot be restarted."
    fi
  fi

  return 0
}

###
# Description: Get RHEL major release.
# Args: none.
# Output: RHEL_MAJOR_RELEASE (int).
###
get_rhel_major_release(){
  RHEL_MAJOR_RELEASE=$(cat /etc/redhat-release | grep -oP '\d.\d*' | cut -d '.' -f1)
}

###
# Description: Restart services.
# Args: $systemctl_restart_services_file.
# Output: file containing the result of service restart.
# Return: 0 if all the services restarted successfully, 1, otherwise.
###
restart_services(){
  # check if the argument exists
  if [ -z "$1" ]; then
    logit "ERROR" "Missing argument in restart_services()."
    return 1
  fi

  # check if the input file exists
  if [ ! -f $1 ]; then
    logit "ERROR" "Cannot find the file containing the services to restart."
    return 1
  fi

  get_rhel_major_release

  # the yaml file containing the result of service restart
  restarted_services_file=$(mktemp $TMP_DIR/restarted_services.XXXXXXX)

  # local vars
  local result=0
  local srv
  local error
  local err
  local return_code

  # read $1
  while IFS= read -r srv
  do
    if [ ! -z $srv ]; then
      if [[ $srv == \#* ]]; then
        logit "INFO" "${srv:1} restart skipped."
        printf -- "    - name: %s\n      succeeded: false\n      return_code: -\n      error_message: Restart skipped. See $CONF_FILE\n" \
          "${srv:1}" >> $restarted_services_file
      else
        if [ $RHEL_MAJOR_RELEASE -ge 7 ]; then
          error=$(systemctl restart $srv 2>&1)
        else
          error=$(service $srv restart 2>&1)
        fi
        return_code=$?
        err=$(echo $error | tr '\r\n' ' ')

        if [ $return_code -eq 0 ]; then
          logit "INFO" "$srv restart succeeded."
          printf -- "    - name: %s\n      succeeded: true\n      return_code: %s\n      error_message: %s\n" \
            "$srv" "$return_code" "$err" >> $restarted_services_file
        else
          logit "ERROR" "$srv restart failed. Error: $error"
          printf -- "    - name: %s\n      succeeded: false\n      return_code: %s\n      error_message: %s\n" \
            "$srv" "$return_code" "$err" >> $restarted_services_file
          result=1
        fi
      fi
    fi
  done < $1

  return $result
}

###
# Description: Reboot the server.
# Args: none.
# Return: 1 on error.
###
reboot_server(){
  error=$(systemctl reboot 2>&1)
  if [ $? -ne 0 ]; then
    logit "ERROR" "Server reboot failed. Error: $error"
    return 1
  fi
}

###
# Description: Update mlocate database.
# Args: none.
# Return: 0 for success, 1 on error.
###
update_mlocate_db(){
  error=$(updatedb -e "$exclude" 2>&1)
  if [ $? -ne 0 ]; then
    logit "ERROR" "updatedb failed. Error: $error"
    return 1
  fi

  return 0
}

###
# Description: Find .rpmsave and .rpmnew files.
# Args: none.
# Output: $rpmnew_file: file containing the list of .rpmsave files.
#         $rpmsave_file: file containing the list of .rpmnew files.
# Return: 0 for success, 1 on error.
###
check_config_changes(){
  # update mlocate database
  update_mlocate_db
  if [ $? -ne 0 ]; then
    return 1
  fi

  local rpmn
  local rpms

  rpmnew_file=$(mktemp $TMP_DIR/rpmnew_file.XXXXXXX)
  rpmsave_file=$(mktemp $TMP_DIR/rpmsave_file.XXXXXXX)

  locate .rpmnew > $rpmnew_file; rpmn=$?
  locate .rpmsave > $rpmsave_file; rpms=$?

  # NOTE: locate returns 1 if it doesn't find the pattern
  if [ $rpmn -gt 1 ] || [ $rpms -gt 1 ]; then
    logit "ERROR" "locate command failed: locate .rpmnew -> $rpmn | locate .rpmsave -> $rpms"
    return 1
  fi

  # format rpmsave and rpmnew files to be used in the yum update report
  sed -i -e 's/^/    - /' $rpmnew_file $rpmsave_file

  return 0
}

###
# Description: Maps 0 or 1 to true or false.
# Args: the digit to convert: 0 or 1.
#       inverse (integer): 0 or 1. If 0, maps 0 to false and 1 to true,
#                                  If 1, maps 0 to true and 1 to false.
# Output: true or false or undefined.
###
binary_to_string(){
  if [ -z $1 ] || [ -z $2 ]; then
    echo "undefined"
    return 1
  fi

  if [ $2 -eq 0 ]; then
    case $1 in
      0) echo "false";;
      1) echo "true";;
      *) echo "undefined";;
    esac
  fi

  if [ $2 -eq 1 ]; then
    case $1 in
      0) echo "true";;
      1) echo "false";;
      *) echo "undefined";;
    esac
  fi

  return 1
}

###
# Description: Report the packages that will be updated, installed and erased by yum update.
# Args: none.
# Output: $dry_run_report_file
# Return: none.
###
dry_run_report(){
  dry_run_report_file=$(mktemp $TMP_DIR/dry_run_report.XXXXXXX)

  # old version of packages that will be updated
  old_version=$(mktemp $TMP_DIR/old_version.XXXXXXX)
  yum update --assumeno | grep "will be updated" | awk '{ print $3 " " $4 }' > "$old_version"

  # new version of packages that will be updated
  new_version=$(mktemp $TMP_DIR/new_version.XXXXXXX)
  yum update --assumeno | grep "will be an update" | awk '{ print $3 " " $4 }' > "$new_version"

  # report of updated packages
  updated_packages=$(mktemp $TMP_DIR/updated_packages.XXXXXXX)
  printf "  updatedPackages:\n" > "$updated_packages"
  join --nocheck-order $old_version $new_version | awk '{ print "  - name: " $1 "  \n    oldVersion: " $2 "  \n    newVersion: " $3 }' >> "$updated_packages"

  # installed packages
  installed=$(mktemp $TMP_DIR/installed.XXXXXXX)
  printf "  installedPackages:\n" > "$installed"
  yum update --assumeno | grep "will be installed" | awk '{ print "  - name: " $3 "  \n    version: " $4 }' >> "$installed"

  # erased packages
  erased=$(mktemp $TMP_DIR/erased.XXXXXXX)
  printf "  erasedPackages:\n" > "$erased"
  yum update --assumeno | grep "will be erased" | awk '{ print "  - name: " $3 "  \n    version: " $4 }' >> "$erased"

  cat "$erased" > "$dry_run_report_file"
  cat "$installed" >> "$dry_run_report_file"
  cat "$updated_packages" >> "$dry_run_report_file"

  rm "$old_version"
  rm "$new_version"
  rm "$updated_packages"
  rm "$installed"
  rm "$erased"
}

###
# Description: Create the yum update report.
# Args: $REBOOT_REQUIRED
#       $SERVICE_RESTART_REQUIRED
#       $MANUAL_SERVICE_RESTART_REQUIRED
#       $SESSION_RESTART_REQUIRED
# Output: $yum_update_report_file
# Return: 0 for success, 1 on error.
###
generate_update_report(){
  # check if the function arguments are provided
  if [ $DRY_RUN -eq 0 ] && ([ -z "$1" ] || [ -z "$2" ] || [ -z "$3" ] || [ -z "$4" ]); then
    logit "ERROR" "Insufficent information to create the yum update result report."
    return 1
  fi

  local uuid=$(uuidgen)
  local now=$(date -u +$DATE_FORMAT)
  local short_hostname=$(echo $HOSTNAME | cut -d"." -f 1)
  get_default_iface_ip

  yum_update_report_file=$REPORTS_DIR/$short_hostname"_"$now".yaml"

  # construncting the yum update report (a yaml file)
  # general section
  printf "hostname: %s\n" $HOSTNAME > $yum_update_report_file
  printf "defaultInterfaceIpAddress: %s\n" $default_iface_ip >> $yum_update_report_file
  printf "uuid: %s\n" $uuid >> $yum_update_report_file

  # yum update section
  printf "yumUpdate:\n" >> $yum_update_report_file
  printf "  date: %s\n" $now >> $yum_update_report_file
  printf "  dryRun: %s\n" $(binary_to_string $DRY_RUN 0) >> $yum_update_report_file
  printf "  rebootAllowed: %s\n" $(binary_to_string $REBOOT_ALLOWED 0) >> $yum_update_report_file
  printf "  autoReboot: %s\n" $(binary_to_string $AUTO_REBOOT 0) >> $yum_update_report_file
  printf "  autoServiceRestart: %s\n" $(binary_to_string $AUTO_SERVICE_RESTART 0) >> $yum_update_report_file

  # the following details are not needed in dry-run mode
  if [ $DRY_RUN -eq 0 ]; then
    printf "  skipServiceRestart: %s\n" $(binary_to_string `expr $REBOOT_REQUIRED \* $AUTO_REBOOT \* $REBOOT_ALLOWED` 0) >> $yum_update_report_file
    printf "  succeeded: %s\n" $(binary_to_string $YUM_UPDATE_RESULT 1) >> $yum_update_report_file
    printf "  rebootRequired: %s\n" $(binary_to_string $1 0) >> $yum_update_report_file
    printf "  serviceRestartRequired: %s\n" $(binary_to_string $2 0) >> $yum_update_report_file
    printf "  manualServiceRestartRequired: %s\n" $(binary_to_string $3 0) >> $yum_update_report_file
    printf "  sessionRestartRequired: %s\n" $(binary_to_string $4 0) >> $yum_update_report_file
    printf "  serverRebooted: %s\n" $(binary_to_string $DO_REBOOT 0) >> $yum_update_report_file

    # Discard this section if the server will be rebooted.
    if [ $DO_REBOOT -eq 0 ]; then
      # service restart
      printf "  serviceRestart:\n" >> $yum_update_report_file
      printf "    restartedServices:\n" >> $yum_update_report_file
      # List of services restarted with systemctl
      if [ $AUTO_SERVICE_RESTART -eq 1 ] && [ $SERVICE_RESTART_REQUIRED -eq 1 ]; then
        cat $restarted_services_file >> $yum_update_report_file
      fi

      # if AUTO_SERVICE_RESTART is disabled, list the services to restart
      printf "    servicesToRestart:\n" >> $yum_update_report_file
      if [ $AUTO_SERVICE_RESTART -eq 0 ] && [ $SERVICE_RESTART_REQUIRED -eq 1 ]; then
        local srv
        while IFS= read -r srv
        do
          if [ ! -z $srv ]; then
            echo "    - $srv" >> $yum_update_report_file
          fi
        done < $systemctl_restart_services_file
      fi

      # get services to be restarted manually
      printf "    servicesToRestartManually:\n" >> $yum_update_report_file
      if [ $MANUAL_SERVICE_RESTART_REQUIRED -eq 1 ]; then
        cat $manual_restart_services_file >> $yum_update_report_file
      fi
    fi

    # Config changes section
    printf "  configChanges:\n" >> $yum_update_report_file
    printf "    rpmsaveFiles:\n" >> $yum_update_report_file

    if [ -f $rpmsave_file ]; then
      cat $rpmsave_file >> $yum_update_report_file
    fi

    printf "    rpmnewFiles:\n" >> $yum_update_report_file

    if [ -f $rpmnew_file ]; then
      cat $rpmnew_file >> $yum_update_report_file
    fi
  fi

  cat "$dry_run_report_file" >> "$yum_update_report_file"

  return 0
}

###
# Description: Send email.
# Args: Destination address.
#       Subject.
#       Email body. This must be a name of a file.
# Return: 0 for success, 1 on error.
# NB. It is assumed that mail is installed and the smtp config is defined on the server.
###
send_email(){
  # Check function's args.
  if [ $# -ne 3 ]; then
    logit "ERROR" "Missing arguments in send_email()."
    return 1
  fi

  # send the email with mail.
  mail -s "$2" $1 < $3

  if [ $? -ne 0 ]; then
    logit "ERROR" "Failed to send email to $1."
    return 1
  fi

  return 0
}

###
# Description: System update.
# Args: none.
# Return: 0 for success, 1 on error.
###
system_update(){
  yum_update
  if [ $? -ne 0 ]; then
    return 1
  fi

  # resinstall m2crypto. No need to exit if this fails.
  reinstall_m2crypto

  read_tracer_output
  if [ $? -ne 0 ]; then
    return 1
  fi

  check_config_changes
  if [ $? -ne 0 ]; then
    return 1
  fi

  # Server reboot decision
  if [ $REBOOT_REQUIRED -eq 1 ] && [ $AUTO_REBOOT -eq 1 ] && [ $REBOOT_ALLOWED -eq 1 ]; then
    logit "INFO" "Server will be rebooted at the end of the system update."
    DO_REBOOT=1
    # Run reboot_server at the end of the script
  else
    DO_REBOOT=0
  fi

  return 0
}

###
# Description: show help text.
###
help_text(){
  echo "
Description:
  The auto-patching script aims to automate the yum update process.

  The script performs the following:
    * Run \"yum update\" or \"yum update --assumeno\".
    * Find out the services that have been affected by the system update.
    * Restart the updated services and reboot the server if needed.
    * Report the service config conflicts that need to be resolved.
    * Generate a summary report of the system update.

Arguments:
  --help: show this message.
  --dry-run: if specified, the script will only run \"yum update --assumeno\" and generate a report of the list of packages that will be updated, installed, or erased. Default value is defined in $CONF_FILE.
  --auto-reboot: if specified, reboot the server if needed and is allowed by the user (see --reboot-allowed below). Default value is defined in $CONF_FILE.
  --auto-service-restart: if specified, restart the services that are affected by the system update. Default value is defined in $CONF_FILE.
  --reboot-allowed: if specified, the script will reboot the server if needed, otherwise, no reboot will occur even if that's instructed by the script and --auto-reboot is enabled. Default value is defined in $CONF_FILE.

Return:
  * 0 for success, 1 on error.
  * Reports are located in $REPORTS_DIR.

Log file:
  $LOG_FILE
"
}

###
# Description: Read arguments.
# Args: script's arguments: $@
###
read_args(){
  DRY_RUN=$dry_run
  REBOOT_ALLOWED=$reboot_allowed
  AUTO_REBOOT=$auto_reboot
  AUTO_SERVICE_RESTART=$auto_service_restart

  if [ "$#" -gt 5 ] ; then
    logit "WARN" "Unexpected number of arguments."
    help_text
    exit 1
  fi

  for arg in "$@"
  do
    case $arg in
      --dry-run)
        DRY_RUN=1
        ;;
      --reboot-allowed)
        REBOOT_ALLOWED=1
        ;;
      --auto-reboot)
        AUTO_REBOOT=1
        ;;
      --auto-service-restart)
        AUTO_SERVICE_RESTART=1
        ;;
      --help)
        help_text
        exit 0
        ;;
      *)
        logit "WARN" "Unknown argument."
        help_text
        exit 1
        ;;
    esac
  done
}

###
# Description: call Main function.
###
read_config_file
if [ $? -ne 0 ]; then
  exit 1
fi

read_args $@

# create the dry-run report.
dry_run_report

# run the system update
if [ $DRY_RUN -eq 0 ]; then
  # Run yum update
  system_update
  if [ $? -ne 0 ]; then
    logit "ERROR" "System update failed."
    exit 1
  fi

  # Only restart services when reboot is NOT going to happen.
  if [ $DO_REBOOT -eq 0 ]; then
    restart_service_if_required
    if [ $? -ne 0 ]; then
      return 1
    fi
  else
    logit "INFO" "Service restart is skipped as the server will be rebooted."
  fi
fi

# create the system update report
generate_update_report $REBOOT_REQUIRED $SERVICE_RESTART_REQUIRED $MANUAL_SERVICE_RESTART_REQUIRED $SESSION_RESTART_REQUIRED
if [ $? -ne 0 ]; then
  logit "ERROR" "Something went wrong. Please check $LOG_FILE"
  exit 1
fi

logit "INFO" "System update report: $yum_update_report_file"

# send email notification
send_email $maillist "[auto-patch] $HOSTNAME | $(date -u +%Y-%m-%d)" $yum_update_report_file
if [ $? -eq 0 ]; then
  logit "INFO" "Patching report has been sent to: $maillist"
fi

# exit now in dry-run mode
if [ $DRY_RUN -eq 1 ]; then
  exit 0
fi

# upload tracer data
upload_tracer_data

if [ $DO_REBOOT -eq 0 ]; then
  exit 0
fi

# DO_REBOOT=1 must be true.
if [ $reboot_by_ansible -eq 0 ]; then
  logit "INFO" "Rebooting the server in 10 seconds..."
  sleep 10
  reboot_server
else
  logit "INFO" "Server reboot via Ansible."
  exit 2
fi
