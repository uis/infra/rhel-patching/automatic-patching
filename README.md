## RHEL auto-patching

The auto-patching script aims to automate the yum update process and report the service config changes that need to be reconciled.

The script performs the following tasks:
* Run system update with `yum update` or in dry-run mode with `yum update --assumeno`.
* Find out the `systemd` services that need to be restarted.
* Check whether the server needs to be rebooted.
* Restart the updated services and reboot the server if required.
* Report the service config conflicts to be resolved after the system update.
* Generate a summary report of the system update.

## Running the script
The script supports 5 arguments:
* `--help`: show this message.
* `--dry-run`: if specified, the script will only run `yum update --assumeno` and generate a report of the list of packages that will be updated, installed, or erased.
* `--auto-reboot`: if specified, reboot the server if needed and is allowed by the user (see `--reboot-allowed` below).
* `--auto-service-restart`: if specified, restart the services that are affected by the system update.
* `--reboot-allowed`: if specified, the script will reboot the server if needed, otherwise, no reboot will occur even if that's instructed by the script and `--auto-reboot` is enabled.

## Contact
[UIS Infrastructure Servers and Storage](mailto:uisinfrastructureserversandstorage@uis.cam.ac.uk)